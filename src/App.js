import React from 'react';
import './App.css';
import Navbar from './compontents/Navbar';
import Todos from './compontents/Todos';
function App() {
  return (
    <div className="App">
      <Navbar />
      <Todos />
    </div>
  );
}

export default App;
