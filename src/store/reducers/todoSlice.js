import { createAsyncThunk, createSlice ,nanoid} from "@reduxjs/toolkit";
//nanoid tạo id ngẫu nhiên
import axios from 'axios'

// Reducer Thunk cách viết mới của Redux
const url = "https://jsonplaceholder.typicode.com/todos?_limit=10"
export const getTodos = createAsyncThunk('todos/getAll', async()=>{
    const res = await axios.get(url)
    return res.data
})

export const addTodos = createAsyncThunk('todos/todoAdded', async title =>{
    const newTodo = {
        id:nanoid(),
        title,
        completed:false
    }
    await axios.post('https://jsonplaceholder.typicode.com/todos', newTodo)
    return newTodo
})

export const deleteItemTodo = createAsyncThunk('todos/delete', async todoId =>{
    await axios.delete(`https://jsonplaceholder.typicode.com/todos/${todoId}`)
    return todoId
})


const todosSlice = createSlice({
    name: "todos",
    initialState: {
        allTodos: [],
    },
    reducers:{
        // addTodos:(state, action) => {
        //     state.allTodos.unshift({
        //         id:nanoid(),
        //         title: action.payload,
        //         completed: false
        //     })
        // }
        // addTodos:{
        //     reducer(state,action){
        //         state.allTodos.unshift(action.payload)
        //     },
        //     prepare(title){
        //         return {
        //             payload:{
        //                 id:nanoid(),
        //                 title,
        //                 completed:false
        //             }
        //         }
        //     },
        // },
        markComplate(state,action){
            const todoId = action.payload
            state.allTodos = state.allTodos.map(todo => {
                if(todo.id === todoId) {
                    todo.completed = !todo.completed
                }
                return todo
            })
        },
        // deleteItemTodo(state,action){
        //     const todoId = action.payload
        //     state.allTodos = state.allTodos.filter(todo => todo.id !== todoId)
        // },
        // Cach viết cũ của redux
        // todosFetched(state,action){
        //     state.allTodos = action.payload  
        // }
    },
    // cách viết mới của Redux
    extraReducers:{
        // get all todo
        [getTodos.pending]: (state,action) => {
            console.log("pedding data");
        },
        [getTodos.fulfilled]: (state,action) => {
            state.allTodos = action.payload
        },
        [getTodos.rejected]: (state,acrion) =>{
            console.log("fall");
        },
        // add todo
        [addTodos.fulfilled]: (state,action) =>{
            state.allTodos.unshift(action.payload)
        },

        // delete todo
        [deleteItemTodo.fulfilled]: (state,action) =>{
            const todoId = action.payload
          state.allTodos = state.allTodos.filter(todo => todo.id !== todoId)
        }
    }
});

// Async action creator, asction and reducer dispatch
// Cách viết cũ của Redux
// export const getTodos = () => async dispatch => {
//     const url = "https://jsonplaceholder.typicode.com/todos?_limit=10"
//     try {
//         const res = await axios.get(url)
//         dispatch(todosFetched(res.data))
//     } catch (error) {
//         console.log(error);
//     }
// }


// reducer
const todosReducer = todosSlice.reducer

// selector 
// export const todoSelector = state => state.todoReducerABC.allTodos
export const todoSelector = state => state.todosReducer.allTodos

// Action export
export const {
    //  addTodos,
     markComplate, 
    //  deleteItemTodo
    } = todosSlice.actions


// Export reducer
export default todosReducer