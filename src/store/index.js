import { configureStore } from "@reduxjs/toolkit";
import todosReducer from "./reducers/todoSlice";

// store
const store = configureStore({
    reducer:{
        todosReducer                        // cách 1 : đây là kiểu viết ES6 => todosReducer:todosReducer
        // todoReducerABC: todosReducer     // cách 2: cách thông thường
    }
})

// export

export default store