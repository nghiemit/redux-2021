import React,{useState} from 'react'
import { addTodos } from '../store/reducers/todoSlice'
import { useDispatch } from 'react-redux'
const TodoForm = () => {
    const [title,setTitle] = useState('')
    const onTitleChange = e =>{
        setTitle(e.target.value)
    }
    const dispatch = useDispatch()
    const onFormSubmit = e =>{
        e.preventDefault()
        if(title !== ""){
            dispatch(addTodos(title))
        }else{
            alert("Empty")
        }
    }
    return (
        <div>
            <form onSubmit={onFormSubmit}>
                <input type="text" name="title" onChange={onTitleChange}/> 
                <input type="submit" value="Add" style={{cursor:"pointer"}}/>
            </form>
        </div>
    )
}

export default TodoForm
