import React ,{useEffect} from 'react'
import TodoForm from './TodoForm'
import {useSelector,useDispatch} from 'react-redux'
import {todoSelector, markComplate, deleteItemTodo, getTodos} from '../store/reducers/todoSlice'

const Todos = () => {
   const todos = useSelector(todoSelector)
   const dispatch = useDispatch()
   const todocompleted = todoId => {
        dispatch(markComplate(todoId))
   }
   const deleteItem = todoId => {
       dispatch(deleteItemTodo(todoId))
   }
   useEffect(() => {
     dispatch(getTodos())
   }, [dispatch])
    return (
        <div className="todo-list">
            <TodoForm />
            <ul>
            {
                  todos.map(todo =>(
                      <li key={todo.id} className={todo.completed ? "completed" : ""}>
                          {todo.title}
                          <input type="checkbox" checked={todo.completed}  onChange={todocompleted.bind(this,todo.id)}/>
                          <button  onClick={deleteItem.bind(this,todo.id)}>Delete</button>
                      </li>
                  )) 
                }
            </ul>
        </div>
    )
}

export default Todos
